---
label: Как запустить триггер для определенного диалога
order: 0
---

Чтобы запустить [триггер](../tunnels/pro/triggers), нужен `clientId`. Его очень просто найти в [логах](../extra/logs).

Найдите нужный диалог в чатах, скопируйте номер телефона или код чата:

![](https://m.bot-marketing.com/wp/wp-content/uploads/2021/06/image-8.png)

Зайдите в раздел Туннели продаж / Логи, в фильтре введите номер телефона или код чата:

![](https://m.bot-marketing.com/wp/wp-content/uploads/2021/06/image-9.png)

Скопируйте из контекста `clientId`:

![](https://m.bot-marketing.com/wp/wp-content/uploads/2021/06/image-10.png)

Мы получили идентификатор: 10379672
