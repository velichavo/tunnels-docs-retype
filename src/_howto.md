---
label: Как заполнять помощь
order: -1000
---
## Форматирование

||| В тексте
```markdown
## Заголовок второго уровня
### Третьего уровня
#### Четвертого уровня
_наклонный текст_ и **жирный текст**
```
||| Результат
## Заголовок второго уровня
### Третьего уровня
#### Четвертого уровня
_наклонный текст_ и **жирный текст**
|||

### Списки

Ненумерованный список:

||| В тексте
```markdown
- элемент списка 1
- элемент списка 2
```
||| Результат
- элемент списка 1
- элемент списка 2
|||

Нумерованный список:

||| В тексте
```markdown
1. элемент списка 1
2. элемент списка 2
```
||| Результат
1. элемент списка 1
2. элемент списка 2 
|||

### Замечание

||| В тексте
```markdown
!!!
Этот текст содержит важную информацию
!!!
```
||| Результат
!!!
Этот текст содержит важную информацию
!!!
|||

### Ссылки

||| В тексте
```markdown
- [Ссылка куда-то](https://example.com)
- [Ссылка на статью](/tunnels/introduction.md)

[!ref](/variables) &mdash; выделенная ссылка на статью
```
||| Результат
- [Ссылка куда-то](https://example.com)
- [Ссылка на статью](/tunnels/introduction.md)

  [!ref](./tunnels/variables)
|||

### Картинки

||| В тексте
```markdown
Картинка из интернета:

![](https://marketing.chat2desk.com/wp/wp-content/uploads/2021/11/image-3.png)

Картинка из репозитория с подписью:

![Картинка из репозитория](/static/images/variables_01.jpg)
```
||| Результат
Картинка из интернета:

![](https://marketing.chat2desk.com/wp/wp-content/uploads/2021/11/image-3.png)

Картинка из репозитория:

![Картинка из репозитория](/static/images/variables_01.jpg)
|||

### Таблицы

||| В тексте
```markdown
Переменная|Описание
---|---
name|Имя клиента
phone|Телефон клиента
```
||| Результат
Переменная|Описание
---|---
name|Имя клиента
phone|Телефон клиента
|||

Компактная таблица:

||| В тексте
```markdown
Переменная | Описание { class="compact" }
---|---
name|Имя клиента
phone|Телефон клиента
```
||| Результат
Переменная | Описание { class="compact" }
---|---
name|Имя клиента
phone|Телефон клиента
|||

### Смайлы

Вот [тут](https://mojee.io/emojis/) все смайлы.

||| В тексте
```markdown
:heart_eyes: :sunglasses: :scream: :heart:
```
||| Результат
:heart_eyes: :sunglasses: :scream: :heart:
|||

### Колонки

В тексте:

```markdown
||| Column 1 :sunglasses:
Column 1 text
||| Column 2 :heart:
Column 2 text 
|||
```

Результат:

||| Column 1 :sunglasses:
Column 1 text
||| Column 2 :heart:
Column 2 text
|||