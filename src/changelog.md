---
order: -2000
---

# История версий

## 2022 год

### Август

#### 2 Августа

- Ко всем запросам со стороны туннелей продаж добавляется заголовок `X-BOTMARKETING-USER`, который определяет пользователя, выполняющего запрос
- Все ранее публичные запросы к API (уведомления, триггеры и т.д.) теперь можно подписать с помощью заголовка `Authorization: Bearer :token`, чтобы увеличить [лимиты](./tariffs/quotas#http). Также теперь такие запросы автоматически подписываются, если выполняются со стороны туннелей продаж через HTTP-запрос.
- Изменены лимиты на запросы к сервису: до 256 запросов в минуту на пользователя с одного IP (подписанные запросы) и до 60 запросов в минуту с одного IP

##### Обновления модуля работы с таблицами Google

- Добавлена возможность указать свой файл аутентификации через параметр `serviceAuthFile`, чтобы изолировать запросы к Google Sheets API
- В качестве `row` теперь можно передавать массив, а не JSON-строку
- Запросы к API привязываются теперь к пользователю, который выполняет запрос. Запросы ограничены до 60 в минуту на пользователя (ранее -- 10 запросов в минуту на файл)
- Добавлен триггер удаления строк
- Добавлен триггер для запроса [batchUpdate](https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/batchUpdate)

### Июль

#### 29 июля

- Добавлена возможность назначать тег по имени с помощью [raw action](./tunnels/actions/raw_action.md)
- Исправлена проблема с обновлением списка полей клиента

#### 4 июля

- Добавлен модификатор `customField`, который позволит получить кастомное поле по номеру
- В новом синтаксисе теперь можно комбинировать пайп (->) и доступ к полю объекта (`@{settings.table.data->where('0', 1).2}`)
- Решение при сборке форматирует все переменные в новый синтаксис. На аккаунте клиента все переменные будут в новом синтаксисе.
- Решена проблема с кастомными полями для решений: теперь их значение будет корректно конвертироваться при установке на аккаунт клиента

### Июнь

#### 30 июня

- Добавлена опция "Создавать параметры для кнопок" в настройках сборки решения, включив которую можно настраивать текст всех кнопок из настроек решения
- Добавлена возможность в названии кнопки использовать переменные
- Релиз [нового синтаксиса переменных](./tunnels/variables/syntax.md)

#### 29 июня

- Добавлен модификатор `date` для преобразования в дату и время
- Добавлен модификатор `inArray` для проверки на то, существует ли значение в массиве

### Апрель

#### 13 апреля

- Добавлена переменная `{chatTriggerId}`, которую нужно использовать как идентификатор на запуск триггера из внешней системы

#### 11 апреля

- Добавлен модификатор `bool` для преобразования значения в булевское

#### 8 апреля

- Добавлена возможность указать цвет кнопки

#### 6 апреля

- Добавлена возможность подставлять динамический код триггера в решении, когда нужно запустить триггер по ссылке. В таком случае код триггера будет подставлен на аккаунте клиента при установке. Это можно сделать вот так: `https://mssg.su/h/%%triggerHash|NbL5U2wy%%/{clientId}`. Конструкция вида `%%triggerHash|NbL5U2wy%%` при запуске на аккаунте, на котором создается решение, превращается в `NbL5U2wy` &mdash; это оригинальный код триггера в этом решении. На аккаунте клиента вместо этой конструкции появляется код его установленного триггера: `https://mssg.su/h/awgDUl0r/{clientId}`.

#### 1 апреля

- Добавлена возможность указать дополнительные ключевые слова в текстовом формате для кнопок

### Март

#### 31 марта

- Добавлена возможность в шаблонах использовать вертикальную черту для указания нескольких вариантов в одном шаблоне, например: `Разработка ПО%|Разработчик%`

#### 30 марта

- Исправлена ошибка, когда номер телефона не определялся из-за латинских символов в конце сообщения

#### 25 марта

- Некоторые визуальные изменения в дизайне настроек решения

#### 24 марта

- Определение нажатой пользователем инлайн кнопки в новом интерфейсе привязывается к идентификатору кнопки, а не только тексту

#### 23 марта

- Добавлен модификатор `only`, который позволит вернуть объект только с указанными полями
- Добавлен модификатор `string`, который преобразует числовое значение в строку (если на входе будет объект, то вернет пустое значение)

#### 15 марта

- Добавлена возможность искать логи по `dialog id`
- Исправлена проблема, когда пустые быстрые ответы отображались в мессенджере

#### 11 марта

 - Добавлен новый тип параметра “Исходящее сообщение” для решений, который позволяет заполнять текст сообщения для разных типов мессенджеров или конкретных подключенных мессенджеров

![](https://m.bot-marketing.com/wp/wp-content/uploads/2022/03/image-5-768x397.png)

#### 10 марта

 - Действие [“Новому клиенту”](./tunnels/actions/contacts_and_chats#new-client) теперь совместимо с решениями. Если в решении используется это действие, то будут использоваться подключенные к решению мессенджеры для создания новых чатов.
 - Переименованы действия:
    - Закрыть диалог -> Закрыть обращение 
    - Перевести диалог -> Передать чат оператору 
    - Отменить платеж -> Закрыть счет
 - Переименованы события:
   - Диалог закрыт -> Обращение закрыто 
   - Диалог переведен на оператора -> Чат передан оператору

Новые кнопки:

- Добавлен новый тип кнопки “Номер телефона”, который позволит в Telegram, Facebook и Viber Public запросить номер телефона у клиента. Для других мессенджеров эта кнопка сработает, если пользователь введет номер телефона вручную в **международном формате**.
- Добавлен новый тип кнопки “Геолокация”, который позволит запросить геолокацию в Telegram и Facebook
- Добавлена переменная `inboxGeolocation`, с помощью которой можно получить данные геолокации: `{inboxGeolocation[lat]}` — широта, `{inboxGeolocation[long]}` — долгота

![Пример использования новых кнопок](https://m.bot-marketing.com/wp/wp-content/uploads/2022/03/image-6.png)

Исправления:

 - Исправлена ошибка с действием “Новому клиенту”, когда клиент существовал на другом канале

#### 2 марта

 - В действия “отправить изображение/видео/pdf” добавлено поле “Подпись”, которое позволяет прикрепить к медиа файлу текст 
 - Действие “отправить сообщение” переименовано в “отправить текст”, также убрано поле “изображение” и “варианты сообщения”. Данные поля останутся, если они уже были использованы ранее.

### Февраль

#### 18 февраля

 - \#5423: Исправлены проблемы с проверкой данных в настройках решения в случае со вложенными параметрами 
 - Исправлена проблема с отображением названия полей в ошибках проверки данных 
 - Повышена производительность отображения туннелей в новом интерфейсе

#### 15 февраля

 - Добавлена переменная `{tunnelSessionHash}` для получения внешнего кода сессии 
 - Добавлено новое событие _Внешнее действие_, с помощью которого можно обрабатывать запросы к работающему туннелю 
 - Добавлено отображение URL в теле шага для URL кнопок

#### 14 февраля

 - Теперь можно будет запустить не более 10 вложенных синхронных туннелей (когда синхронный туннель запускает другой синхронный туннель). 
 - Добавили модификаторы `firstMatchingPattern`, `firstMatchingPatternI`, `firstNotMatchingPattern`, `firstNotMatchingPatternI`. С помощью них можно достать объект из коллекции, одно из полей которого является шаблоном и какое-то значение соответствует этому шаблону. По сути, это инверсия `whereLike`. Пример использования: `{{ $$items | firstMatchingPatternI pattern $inboxText }}`. В поле `pattern` объектов внутри `$items` должен быть шаблон.

#### 8 февраля

 - Добавлен модификатор `diffForHumans` для отображения разницы между датами в виде количества минут, секунд и т.д.

### Январь

#### 27 января

 - Добавлено ключевое слово `\b` для шаблонов: позволяет мэтчить границу слова 
 - Добавлены регистронезависимые модификаторы поиска `whereLikeI` и `whereNotLikeI`

#### 14 января

 - Теперь действия можно скопировать и вставить в нужный шаг, а не в тот же, в котором оно находилось 
 - Мелкие исправления в новом интерфейсе

#### 12 января

 - Добавлен модификатор `phoneNumber`, который позволит достать номер телефона из строки и вернуть его в формате `79205550123`

## 2021 год

### Декабрь

#### 22 декабря

 - Добавлен модификаторы `match` и `matchAll` для проверки соответствия строки регулярному выражению 
 - Добавлен модификатор `filter`, который позволит отфильтровать пустые значения в коллекции

#### 9 декабря 

 - Добавлена URL кнопка в новый интерфейс: можно добавить кнопку с отслеживанием перехода (тогда можно менять логику туннеля, но ссылка подменяется), либо без отслеживания (ссылка не подменяется, но в туннеле вы не узнаете, перешел ли пользователь по ссылке)

#### 7 декабря 

 - Исправлена проблема с действием Новому клиенту, когда клиент уже существует

### Ноябрь

#### 25 ноября 

 - Добавлен модификатор `buildHttpQuery`, который позволяет преобразовать массив в `query string`

#### 22 ноября 

 -Для параметра _Данные из таблицы Google_ в настройках решения добавлена опция “Частота обновления”, которая позволит ускорить или замедлить обновление. Также добавлено поле `nextUpdateAt`, которое будет определять дату ближайшего обновления.

#### 9 ноября 

 - Добавлен модификатор `sortBy` и `sortByDesc` для сортировки коллекции объектов

#### 1 ноября 

 - Исправлена ошибка с обработкой фрагмента в URL (URL-кнопки, HTTP-запросы)

### Октябрь

#### 29 октября 

 - Добавлена переменная `messengerAccount`: она содержит аккаунт текущего мессенджера, в котором происходит общение (группа telegram, номер телефона whatsapp и т.д.)
 - Изменён механизм обновления переменных, когда значением обновляемой переменной является объект со своими полями. Например, раньше, когда мы обновляли переменную `item => {{ $$items | get $$i }}`, то, если переменная `item` уже существует, в нее могли быть добавлены поля нового объекта, либо старые поля не перезаписывались, если они отсутствовали в новом объекте. Теперь переменная `item` будет полностью перезаписана новыми данными. При этом, если использовать вложенные поля (например вот так: `item.foo => 'bar', item.baz => 1`), то эти поля будут добавлены к объекту, как и раньше.

#### 27 октября 

 - Добавлена переменная `Входящее: текст цитаты (inboxQuote)`. Эта переменная будет содержать текст цитаты, если клиент ответил на какое-то сообщение 
 - Добавлена переменная `Групповой чат: пользователь (groupChatUser)`. Эта переменная будет содержать номер телефона пользователя, который пишет в группу WhatsApp

#### 19 октября 

 - Туннель теперь будет получать видео от пользователей в переменной `inboxAttachment`

#### 14 октября 

 - В логи добавлен вывод решения

#### 13 октября 

 - Добавлен обработчик события “Счет оплачен”, который можно использовать в туннеле. Это событие будет срабатывать, когда счет, выставленный туннелем, переходит в статус “Оплачен”. Событие в глобальной ветке срабатывает для любого счета. 
 - В параметре _Данные из таблицы Google_ добавлена переменная `sheetIds`, из которой можно получить идентификатор листа по его названию, например: `{settings[table.sheetIds.Лист 1]}`

### Сентябрь

#### 29 сентября 

 - Исправлена ошибка, когда браузер вылетал из-за того, что в параметре с типом _Данные из таблицы Google_ слишком много данных

#### 27 сентября 

 - Добавлена платежная система GPWebpay 
 - Добавлены модификаторы `first` и `last` для коллекций

#### 24 сентября 

 - Для модификатора `where` предусмотрен вариант, когда на вход подается объект. Тогда коллекция фильтруется по полям этого объекта.

#### 23 сентября 

 - В сборке решения, если оригинальное решение использует модули, при этом оригинальное решение использует параметр, который так же определен в модуле, то параметр из оригинального решения имеет приоритет и его настройки не наследуются из модуля

#### 17 сентября 

 - Добавлен триггер запуска “При совпадении глобальной ветки”, чтобы можно было отключить обработку глобальных веток туннеля 
 - Если принудительно останавливается туннель, который запускал синхронные сессии, то синхронные сессии тоже останавливаются 
 - Добавлен перевод многих строк в логах

#### 15 сентября 

 - Исправлена ошибка, когда ключевое слово могло не срабатывать в некоторых случаях (#8777)
 - В расширенном синтаксисе при использовании текстового параметра с кавычками можно использовать саму кавычку вот так: `{{ "Привет, \"мир!\"" }}`
 - В расширенном синтаксисе можно выводить названия переменных, вместо их значения вот так: `{{ \$$notVariable }}`

Решения:

 - Добавлена возможность скачать исходник решения в виде файла json 
 - При установке копии решения теперь не будут дублироваться платежные системы 
 - При установке копии исходного решения не будут дублироваться ресурсы, которые используются в этом решении, но не являются его частью (например, модули)

#### 14 сентября 

 - Добавлен тип параметра ключ-значение для настроек решения

### Август

#### 31 августа 

 - Добавлено условие “соответствует шаблону” для проверки строк на соответствие простым шаблонам вроде “%привет%”

#### 26 августа 

 - Варианты ответов в глобальных ветках сортируются по приоритету среди всех глобальных веток, а не в пределах одной ветки. Таким образом, запускаться будет туннель, для которого совпадение является наиболее конкретным.

#### 16 августа 

 - Добавлен тип скидки _Скидка в валюте_, с помощью него можно делать скидку, например, 100 руб 
 - Если скидка указана на заказ, то скидка применяется ко всему заказу, а не к каждому элементу в отдельности. Скидка при этом распределяется по всем товарам

#### 9 августа 

 - Добавлен модификатор `sha256`

#### 6 августа 

 - Изменен внешний вид редактора ключ\значение

#### 4 августа 

 - Добавлены события в туннель “Диалог закрыт” и “Диалог переведен на оператора” 
 - В магазине отрицательные скидки теперь не могут быть, даже если в акции указана конкретная цена выше цены товара

### Июль

#### 29 июля 

 - Добавлена автоматическая подстановка API токена Chat2Desk при совершении HTTP запросов к API Chat2Desk через действие HTTP запрос. Токен добавляется, если URL запроса соответствует запросу к API Chat2Desk, при этом заголовок `Authorization` не заполнен. 
 - Удаленные операторы теперь не будут отображаться в списке операторов 
 - Добавлен модификатор `pluck`, который позволяет извлечь свойство из списка объектов, опционально по ключу

#### 16 июля 

 - Задержка до точной даты и времени получает дату с точностью до секунды в случае, если дата рассчитывается

#### 12 июля 

 - \#7137: добавлена переменная “Группы с операторами онлайн” чтобы проверять, есть ли операторы онлайн в указанной группе

### Июнь

#### 30 июня 

 - Добавлен триггер [запуска туннеля](./tunnels/start_stop) “При создании нового чата оператором” 
 - Создание чата оператором больше не запускает туннель, для которого активен триггер “По обращению” 
 - #7165: Добавлена возможность [перехватывать сообщения оператора в туннелях](./tunnels/pro/handling_outbox) 
 - Исправлены проблемы в решениях, когда нарушалась структура туннеля 
 - Исправлена проблема с кнопками в виджетах, которые привязаны к решению

#### 22 июня 

 - При вызове триггера с помощью POST, передаются параметры из тела запроса 
 - Добавлен специальный эндпоинт триггеру для обработки событий от [mango office](./integrations/mango_office)

#### 05 июня 

 - #6727: Исправлены проблемы с данными из таблиц Google в случае одновременного доступа к ним

#### 04 июня 

 - Изменился адрес, с которого выполняется отправка уведомление на почту: notifications@mssg.su

### Май

#### 31 мая 

 - Исправлена ошибка с остановкой туннеля со скоупом 
 - Добавлены модификаторы коллекций `groupBy`, `keyBy` и `keys` 
 - Добавлены переменные `snPostId`, `snPostUrl` — идентификатор поста социальной сети и ссылка

#### 26 мая 

 - Добавлены [глобальные обработчики входящих](./tunnels/pro/global_events): если отметить ветку вариантов ответов как глобальную, мы будем проверять все необработанные входящие в этой ветке и запускать туннель, если появляется совпадение 
 - Добавлен фильтр в списке туннелей продаж для отображение туннелей с глобальными ветками

#### 13 мая 

 - Добавлены модификаторы `int` и `float` для преобразования строки в целое число и в число с плавающей точкой соответственно (из строки будут удалены все символы, кроме цифр и некоторых других знаков)

#### 11 мая 

 - Действие “Закрыть диалог” теперь может закрыть новое обращение (при этом диалог будет назначен на предыдущего оператора)
 - Fix #6523: исправлено некорректное отображение флага запуска по ключевому слову в настройках туннеля

### Апрель

#### 29 апреля 

 - Переработана передача параметров по умолчанию у триггера для более адекватной работы. Параметры с точкой будут создавать объект.

#### 26 апреля 

 - Добавлен булевый модификатор `contains ({{ $$str | contains "abc" }})`
 - Добавлено описание булевый модификаторов в справку

#### 23 апреля 

 - #6361: Добавлен модификатор `md5`

#### 20 апреля 

 - Fix #6301: при использовании модификатора `expand` не форматировать результат в строку, а сохранять тип данных 
 - Fix #6293: не обновлять настройки запуска туннеля в решениях только для основного туннеля, остальные туннели обновлять всегда

#### 2 апреля 

 - Рефактор заказов: можно создать заказ без товара, но с указанием решения

### Март

#### 31 марта 

 - Добавлен триггер запуска туннеля _при переводе на оператора_ (туннель автоматически перезапускается)
 - Обновлена страница логов: добавлены колонки туннеля и шага, подсветка ошибок, перевод текста

#### 25 марта 

 - Добавлена возможность в расширенном синтаксисе использовать вертикальную черту как `\|`
 - Добавлены модификаторы для фильтрации по дате:
   - `whereBefore` 
   - `whereBeforeOrSame` 
   - `whereAfter` 
   - `whereAfterOrSame` 
   - `whereDateBetween`
 - Добавлен модификатор `whereNotLike` 
 - Добавлен модификатор для даты `endOf` и `startOf` для округления даты до определенного интервала 
 - Теперь можно добавлять теги до 20 символов длиной

#### 19 марта 

 - Настройка Основной туннель в решениях позволяет вынести настройки запуска в решение, а также добавляет кнопки быстрого запуска туннеля 
 - Параметры, привязанные к другому решению, теперь нельзя перетащить 
 - Небольшая переработка перетаскивания параметров, чтобы дать больше контроля

#### 5 марта 

 - Возможность загружать все данные в параметре Google таблиц; такие данные группируются по названию листа 
 - Возможность зафиксировать диапазон значений в настройках параметра и запретить его изменять 
 - Выбирается первая вкладка в настройках решения, если не выбрана никакая 
 - Переведены логи 
 - Добавлен модификатор `whereLike` 
 - Добавлен модификатор коллекций `take`

#### 3 марта 

 - Для действия [Запустить туннель](./tunnels/actions/auxiliary#tunnel-start) в синхронном режиме добавлена возможность изменить переменную, которая устанавливается по завершению туннеля

### Февраль

#### 25 февраля 

 - Исправлена ошибка в туннелях, когда при проверке на цикл не учитывалась ветка условий/ответов 
 - Для параметров решения, которые унаследованы от других решений, добавлена пометка в списке в виде иконки, а также в виду сообщения в редакторе параметра

#### 21 февраля 

- Добавлен модификатор `values` для коллекций
- Добавлен модификатор `upper` для строк

#### 19 февраля 

- Добавлена возможность указывать несколько одинаковых мессенджеров в настройках решения
- В настройках решения транспорты группируются по мессенджеру (например, `wa_infobip` — это также WhatsApp)
- В настройках минилендинга, который привязан к решению, можно выбрать нужный аккаунт для каждого мессенджера

#### 17 февраля 

- Заменили логотип Yandex Kassa на ЮKassa