---
label: Создание решений
order: -90
icon: <svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="currentColor" d="M13 3v8h8V3h-8M3 21h8v-8H3v8M3 3v8h8V3H3m10 13h3v-3h2v3h3v2h-3v3h-2v-3h-3v-2Z"/></svg>
---

[!ref](./start.md)
[!ref](./parameters.md)
[!ref](./key-value.md)
[!ref](./gtables.md)
[!ref](./convert.md)
[!ref](./solution_as_a_module.md)