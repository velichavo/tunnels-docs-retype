---
label: Квоты и лимиты
order: -10
---

В системе есть множество различных квот и лимитов. Каждый лимит работает независимо от другого, и в разных ситуациях ограничивать производительность может любой из них.

Большинство этих параметров можно изменить.

Название|Ограничение|Действие в случае превышения 
-|-|-
Отправка сообщений|15 сообщений в секунду|Сообщения отправляются с задержкой
Переходы по шагам в туннелях|1000 шагов в час на клиента|Все туннели перестают работать до сброса счетчика
Рассылки в туннелях|300 сессий в минуту|Сессии запускаются в разное время. Например, если у вас 1000 клиентов<br>и вы хотите отправить сообщение в 10:00, то фактически сообщения будут отправлены в период с 10:00 до 10:04
Активность на аккаунте пользователя|8 одновременных задач|Выполнение задач откладывается во времени
Количество различных переменных|Не ограничено|
Общий объем переменных|Не более 32КБ|Новые переменные не сохраняются
Отправка системных сообщений|20 сообщений в минуту|Системные сообщения сверх лимита не отправляются
Время ожидания ответа сервера при HTTP-запросе|10 секунд|Возвращается ошибка timed_out
Максимальный размер тела ответа HTTP запроса|32КБ|Возвращается ошибка response_body_too_big
Запуск синхронных туннелей|10 уровней вложенности|Туннель будет завершен с ошибкой

## HTTP-запросы к сервису и API {#http}

Для авторизованных пользователей лимит составляет 256 запросов в минуту c одного IP. Для неавторизованных &mdash; 60 запросов в минуту c одного IP.

При выполнении запросов из туннелей продаж к сервису (например, вызов триггера по URL), запросы автоматически подписываются пользователем, который их отправляет.