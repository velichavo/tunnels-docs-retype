# Тарификация решений из каталога

#### Из чего складывается стоимость решения?

Стоимость решения состоит из стоимости настройки решения и ежемесячной абонентской платы за него.

Если в карточке решения нет обозначенной стоимости настройки, то это означает, что настройка производится бесплатно. Абонентская плата за решения взымается отдельно от туннелей продаж.

#### Как продлить решение?

Для продолжения работы решения вам необходимо оплатить следующий период пользования данным решением. Для этого достаточно вовремя оплачивать счета, которые мы высылаем в чате.

#### Как заказать доработку для решения?

О том, как заказать индивидуальную техническую разработку решения или доработку существующего, можно узнать в статье “[Заказ индивидуального решения](../catalog/order_solution)“.

#### Что такое лимиты и как они считаются?

Лимиты на решение – это месячное ограничение по количеству уникальных чатов, которое может обрабатывать данное решение. Лимиты для решений считаются отдельно от лимитов для туннелей продаж. При этом по ним действую такие же правила, как в случае с лимитами туннелей продаж. 

[!ref Подробней о лимитах для туннелей продаж](../tariffs/new_tariffs)

#### Что будет, если я не заплачу за решение вовремя?

Если вовремя не внести ежемесячную абонентскую плату за решение, то оно замораживается через 7 календарных дней после завершение текущего оплаченного периода: туннели приостанавливают свою работу, но не завершаются.
Еще через 7 календарных дней после этого туннели полностью завершаются. После этого восстановить их будет невозможно.

#### Что будет, если превысить лимит по решению?

На данный момент превышение лимитов никак не повлияет на работу решения.
После запуска работы биллинга Chat2Desk, на превышение лимитов будут действовать те же правила, что и на лимиты по туннелям продаж.
[!ref Подробней о превышение лимитов в туннелях](../tariffs/new_tariffs)

#### Я хочу остаться на своем тарифе и не хочу платить больше суммы тарифа. Что делать?

В таком случае, после превышения лимитов включается замедление трафика на вашем аккаунте.

#### Я не использовал весь лимит. Могу ли я вернуть деньги или перенести их на следующий месяц?

К сожалению, переносить выделенные лимиты на следующий месяц нельзя. Так же нельзя вернуть часть средств за неизрасходованную часть лимитов, так как фактически они были переданы в полном объеме при оплате решения.

#### Как понять, какое мне нужно решение?

Чтобы определиться, с каким решением вам начать работать, напишите нам в техническую поддержку ваш запрос, и мы вам поможем подобрать оптимальное решение.

#### Как работает замедление и когда оно включается? Как понять, уже работает оно у меня или еще нет?

Информация о наличии замедления трафика будет отображаться на главной странице туннеля продаж в блоке вашего решения. Также вам на e-mail придет уведомление о старте замедления.

Замедление трафика – это ситуация, когда любое действие внутри туннелей продаж будет идти с задержкой. Например, задержка на обработку входящего сообщения составит 1 минуту.

#### Как убрать замедление?

Чтобы убрать замедление трафика, вам необходимо перейти на следующий тариф, количество лимитов по которому соответствует вашей реальной нагрузке.