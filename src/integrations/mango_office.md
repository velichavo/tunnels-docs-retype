---
label: Mango Office
order: -10
---

## Обработка событий по звонку

Вы сможете с помощью триггера выполнять какие-либо действия в зависимости от состояния звонка. Например, если клиент не дозвонился, вы сможете отправить ему сообщение в WhatsApp.

Для подключения нужно будет создать триггер на нашей стороне и внешнюю систему на стороне Mango.

### Как создать триггер

Для начала вам нужно создать [триггер](../tunnels/pro/triggers). Mango будет направлять запросы на этот триггер вместе с данными звонка, которые описаны в [документации api](https://www.mango-office.ru/support/integratsiya-api/spisok_integratsiy/obshchie_voprosy_po_api_vats_mango_office/). Например, чтобы получить номер телефона, нужно использовать переменную `{query[from.number]}`.

Добавим действие [Новому клиенту](../tunnels/actions/contacts_and_chats#new-client), как на скриншоте ниже:

![](https://m.bot-marketing.com/wp/wp-content/uploads/2021/06/image-1.png)

Когда мы хотим отправить сообщение, если входящий вызов не состоялся, то нужно добавить условие на это действие:

![Входящий вызов не состоялся](https://m.bot-marketing.com/wp/wp-content/uploads/2021/06/image-11.png)

И добавим само сообщение внутри этого действия:

![](https://m.bot-marketing.com/wp/wp-content/uploads/2021/06/image-4.png)

Теперь, если причина завершения вызова не было нормальное завершение (положил трубку оператор или звонящий), то клиенту в WhatsApp автоматически отправится сообщение или HSM-шаблон.

### Как настроить Mango

Для начала нужно сформировать ссылку для внешней системы. У созданного вами триггера появилась ссылка, она выглядит вот так:

![](https://m.bot-marketing.com/wp/wp-content/uploads/2021/06/image-6.png)

Вам нужен параметр `{clientId}`, вы можете получить его [из логов](../problem_solving/trigger_client_id). Допустим, ваш `clientId` = 1234567, тогда итоговый адрес внешней системы будет выглядеть вот так:

```
https://mssg.su/h/8BgUOQbX/1234567/vbpx/
```

Откройте раздел _Интеграции_ / _API коннектор_:

![](https://m.bot-marketing.com/wp/wp-content/uploads/2021/06/image-5.png)

Добавьте внешнюю систему и впишите адрес:

![](https://m.bot-marketing.com/wp/wp-content/uploads/2021/06/image-7.png)

Отключите все события, кроме `events/summary`.

!!!
Мы будем отклонять любые события, кроме summary
!!!

Готово! Теперь мы будем отправлять сообщение, если клиент не смог дозвониться.

## Список параметров

Параметр|Значение { class="compact" }
--------|--------
from.number|Номер телефона звонящего
line_number|Номер телефона, на который поступил звонок
disconnect_reason|Причина отклонения (см. [документацию Mango](https://www.mango-office.ru/support/integratsiya-api/spisok_integratsiy/obshchie_voprosy_po_api_vats_mango_office/))
call_direction|Направление звонка<br>1 — входящий вызов

Другие параметры смотрите в описании события `summary1` в [документации Mango](https://www.mango-office.ru/support/integratsiya-api/spisok_integratsiy/obshchie_voprosy_po_api_vats_mango_office/).

