---
label: Переменные в URL
order: -100
---

В действиях [HTTP запрос](../actions/auxiliary#http-request) и [Вебхук](../actions/auxiliary#webhook) необходимо указывать ссылку на внешний сервис. Часто бывает необходимо использовать переменные как для формирования основной части URL, так и для параметров, например:

```
@{settings.my_service_domain}/api/process?id=@{$id}
```

Здесь доменное имя мы берем из настроек, а также передаем параметр `id`. Параметры после вопросительного знака называются _query string_ и эти параметры необходимо кодировать особым образом. Когда вы используете переменные в параметрах, мы их кодируем за вас. Но это еще не все: в параметрах вы можете передать сложные объекты и мы их корректно подставим.

Например, объект `filter = {"dateFrom":"2020-18-12","status":["completed","sent-to-delivery"]}` и объект `pagination = {"page":1,"limit":100}`. Эти объекты можно вставлять напрямую в URL:

```
https://example.com?filter=@{$filter} => https://example.com?filter[dateFrom]=2020-18-12&filter[status][]=completed&filter[status][]=sent-to-delivery

https://example.com?@{$pagination} => https::/example.com?page=1&limit=100
```