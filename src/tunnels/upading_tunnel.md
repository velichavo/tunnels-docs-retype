---
label: Изменение туннеля во время работы
order: -60
---

=== **Перемещение шагов**

Если у вас изначально была последовательность шагов А-Б-В, а вы, допустим, поменяли на последовательность В-А-Б. В таком случае, все туннели, которые были на шаге В при его выполнении перейдут дальше на шаг А (т.е. вернутся в начало).

=== **Перемещение действий**

Если у вас в шаге А было действие, а потом вы его перенесли в шаг Б, то может возникнуть такая ситуация. У клиента может выполниться действие два раза, сначала на шаге А, а потом на шаге Б, когда туннель на него перейдет.

=== **Удаление шагов**

При попытке активировать шаг, который во время редактирования туннеля был удалён, мы попробуем активировать следующий шаг от текущего с указанной для него задержкой. Если текущий шаг также был удалён, туннель завершается с ошибкой (-8).

=== **Изменение задержек**

Если вы меняете задержку в шаге, то это не поменяет время уже запланированных шагов. Система работает так, что если у шага есть задержка, то он планируется на определенное время в зависимости от задержек, и изменение этой задержки уже не влияет на запланированные шаги.

=== **Изменение графика работы**

Шаги в пределах графика могут быть отложены на время продолжительностью вплоть до суток. Если вы, допустим, не работали в воскресенье, а потом вдруг заработали, то шаги не запустятся моментально, а только в то время, на которое они были запланированы (в зависимости от настроек, скорее всего это будет понедельник).
===