---
label: Кнопки в мессенджерах
order: -140
---

Кнопки поддерживаются следующими мессенджерами:

Тип кнопки|Telegram|Viber Public|VK|Facebook|Онлайн чат
-|:-:|:-:|:-:|:-:|:-:
Быстрый ответ (или просто кнопка)|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:
Инлайн кнопка|:white_check_mark:|:white_check_mark:|:x:|:white_check_mark:|:x:
Инлайн ссылка|:white_check_mark:|:white_check_mark:|:x:|:white_check_mark:|:x:
Номер телефона|:white_check_mark:|:white_check_mark:|:x:|:white_check_mark:|:x:
Геолокация|:white_check_mark:|:x:|:x:|:white_check_mark:|:x:

Если мессенджер не представлен в таблице, то он не поддерживается.

## Геолокация

Если пользователь присылает геолокацию, она будет доступна из переменных `@{inboxGeolocation.lat}` (широта) и `@{inboxGeolocation.long}` (долгота).