---
label: Скоуп
order: -80
---

В одном чате с клиентом может работать только одна сессия определенного туннеля. То есть, если туннель уже запускался, то повторно запустить его уже не получится.

Но бывают такие ситуации, когда в одном чате необходимо запускать несколько туннелей. Для чего же? Ну, например, если вам нужно обрабатывать несколько счетов. Клиенту выставляются счета в чате и нужно запускать управляющие туннели по каждому счету. Это могут быть уведомления с автоматической отменой счета или отправкой информации во внешний сервис.

Для действий [Запустить туннель](../actions/auxiliary#tunnel-start) и [Остановить туннель](../actions/auxiliary#tunnel-stop) можно указывать поле _Скоуп_. В это поле можно заполнить строку с использованием переменных. Как это работает? Скоуп — это группа туннеля. По умолчанию, это поле пустое и это группа по умолчанию. Различные значения в этом поле формируют различные группы туннелей, а внутри группы может быть запущен только одна сессия туннеля.

Для примера со счетами, в этом поле нужно проставить `invoice:{invoiceId}`. Так как код счета уникальный, то можно будет запустить несколько сессий туннеля, но при этом нельзя запустить туннель для одного и того же счета несколько раз.

!!!
Если вы указали скоуп при запуске, то такой же скоуп нужно указывать и для остановки туннеля.
!!!