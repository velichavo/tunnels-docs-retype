---
label: Туннель как функция
order: -70
---

Очень часто бывает так, что какой-то функционал повторяется и нам приходится копировать шаги и целые блоки. Копирование при этом на самом деле не очень хорошая штука, ведь любые изменения в функционале блоков нужно производить во всех местах, куда вы его скопировали. На помощь приходят туннели как функции.

Основной смысл в том, что вы можете запустить любой туннель из любого места, при этом текущий контекст _ждет завершения туннеля_. Но это еще не все. В самом туннеле можно определить переменную `result` и, внимание, эта переменная вернётся в то место, откуда запускался туннель! То есть можно вернуть результат и использовать его дальше.

Рассмотрим на примере. Есть два туннеля: основной и тестовый. Тестовый туннель будет запускаться как функция и возвращать результат. В основном туннеле добавим запуск тестового. В действии [Запустить туннель](../actions/auxiliary#tunnel-start) есть поле Тип запуска, в нем нужно выбрать _Синхронный_:

![](../../static/images/tunnel-as-a-function.png)

И сразу после запуска [туннеля отправим сообщение](../actions/sending_messages#send-text):

![](../../static/images/tunnel-as-a-function2.png)

Мы просто отправляем содержимое [переменной](../variables/custom_variables) `result`.

Создадим тестовый туннель. В нем будет один шаг, добавим в него действие [обновления переменной](../actions/auxiliary#update-variables):

![](../../static/images/tunnel-as-a-function3.png)

Что же мы получим по итогу? После запуска основного туннеля, он начнет выполнять действие запуска тестового туннеля. Он запустит туннель, _приостановит свою работу_ и передаст управление тестовому туннелю. Тестовый туннель перейдет на свой единственный шаг, назначит переменную `result` и завершится.

Тут начинается самое интересное. Когда синхронный туннель завершается, он возвращает управление на то место, где запускался. В нашем случае &mdash; это основной туннель, он продолжает выполнять свои действия. Дальше только отправка сообщения. В самом сообщении отправляется значение переменной `result`, значение которой тестовый туннель установил в “Привет, мир!”. В итоге мы отправляем сообщение, которое определил тестовый туннель.

Возвращать можно любые данные. Или не возвращать — это не обязательно.

!!!
Туннель, запущенный синхронно, игнорирует настройки фильтра по аккаунтам и график работы.
!!!

!!!
Можно запустить не более 10 уровней вложенных синхронных туннелей.
!!!

### Сохранять значение в другую переменную { #store-value-in-another-variable }

По умолчанию, результат выполнения туннеля сохраняется в переменную `result`, но это можно изменить. В действии [Запустить туннель](../actions/auxiliary#tunnel-start) есть поле _Переменная для сохранения_. Укажите в нем название переменной:

![](../../static/images/store-value-in-another-variable.png)

!!!
Хоть в данном примере результат и будет сохранен в переменную `menu`, в запускаемом туннеле все равно нужно изменять переменную `result`, чтобы вернуть значение.
!!!

## Обработка входящих { #incoming-processing }

Туннель как функция может обрабатывать входящие сообщения. Для этого должно быть выполнено несколько условий.

Первое. Первый шаг туннеля должен быть вариантами ответов:

![Здесь мы распознаем два варианта и возвращаем номер как результат.](../../static/images/incoming-processing.png)

Второе. Туннель должен запускаться на варианте ответа:

![](../../static/images/incoming-processing2.png)

Мы здесь задаем вопрос, а потом на любом входящем запускаем туннель, который производит само распознание. А дальше уже проверяем значение, которое он вернул.

!!!
Обратите внимание, что в данном примере есть шаг _Не распознали_, на который мы попадем, если переменная `result` не удовлетворяет заданным условиям. То есть это обработчик _ИНАЧЕ_.
!!!

Понятное дело, что это простой пример и можно обойтись без таких сложностей, однако если ветки какие-то сложные, с дополнительными проверками, то такой обработчик вполне может пойти на службу и упростить жизнь.