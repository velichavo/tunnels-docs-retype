---
label: Веб-хуки
order: -100
---

Веб-хук – это URL, на который отправляется POST-запрос с некоторыми данными при различных событиях. Данные форматированы в json и передаются в теле запроса.

В ответ на запрос ваш сервер должен вернуть статус 200 или 202. В противном случае, запрос будет повторно отправлен через 5 минут. Если и этот запрос будет неудачным, то наша система сделает последнюю попытку еще через пол часа.

Наша система не гарантирует четкую последовательность вызова хуков по времени.

Пример данных:

```
{
"timestamp":1519918073,
"event":"widgetSubmit",
"payload": { ... }
}

```

 - `timestamp` – это время в формате UNIX-timestamp, когда произошло событие 
 - `event` – событие 
 - `payload` – данные события

Определены следующие типы событий:

Событие|Описание|Тип данных { class="compact" }
-------|--------|----------
widgetSubmit|Клиент написал с помощью виджета |WidgetSession
chainNodeActivated|Туннель перешел на шаг|ChainSession
billed|Выставлен счет|Invoice
invoiceExpired|Истек срок жизни счета|Invoice

## Типы данных

### Client (Клиент)

Поле|Тип|Описание { class="compact" }
----|---|--------
id|int|Идентификатор в системе туннелей
externalId|int|Идентификатор на chat2desk
name|string|Имя
phone|string|Телефон
avatar|string|URL аватара

### Transport (Мессенджер)

Поле|Тип|Описание { class="compact" }
----|---|--------
id|int|
name|string|Название транспорта

### Channel (канал)

Поле|Тип|Описание { class="compact" }
----|---|--------
id|int| 
externalId|int|Код канала на chat2desk
name|string| 	 
phone|string| 	 

### Dialog (Диалог)

Поле|Тип|Описание { class="compact" }
----|---|--------
externalId|int|Код диалога на chat2desk

### Message (Сообщение диалога)

Поле|Тип|Описание { class="compact" }
----|---|--------
externalId|int|Код сообщения на chat2desk

### Chain (Туннель)

Поле|Тип|Описание { class="compact" }
----|---|--------
id|int|
code|string|Код туннеля
name|string|Название
status|int|-2 — удалена, -1 — черновик, 0 — неактивна, 1 — активна

### Node (Шаг туннеля)

Поле|Тип|Описание { class="compact" }
----|---|--------
id|int| 	 
title|string|Заголовок шага

### ChainSession (Сессия туннеля)

Поле|Тип|Описание { class="compact" }
----|---|--------
id|int|Идентификатор сессии
chain|Chain|Туннель
client|Client|Клиент
transport|Transport|Транспорт сессии
channel|Channel|Канал сессии
dialog|Dialog|Чат
message|Message|Сообщение
status|int|0 — активна, 1 — завершена, 2 — остановлена
node|Node|Текущий шаг
ref|string|Пользовательский параметр (передается в ссылке на старт туннеля)
createdAt|datetime|Дата старта сессии
finishedAt|datetime|Дата завершения сессии
lastMessageSentAt|datetime|Дата отправки сообщения туннелем
lastInbox|string|Текст последнего входящего сообщения
lastInboxAt|datetime|Дата входящего сообщения

### WidgetSession (сессия виджета)

Поле|Тип|Описание { class="compact" }
----|---|--------
id|int|Идентификатор сессии
widget|Widget|Виджет
client|Client|Клиент
transport|Transport|Транспорт сессии
channel|Channel|Канал сессии
code|string|Код сессии
phone|string|Номер телефона, введённый клиентом
utm|string|Захваченные UTM-метки
agent|string|`HTTP User-Agent`
referer|string|`HTTP Referer`
createdAt|datetime|Дата создания сессии
updatedAt|datetime|Дата обновления сессии

### Invoice (счет)

Поле|Тип|Описание { class="compact" }
----|---|--------
id|int|Идентификатор счета
number|int|Номер счета
hash|string|Код счета
status|InvoiceStatus|Статус счета
amount|int|Сумма счета
paidAmount|int|Оплаченная сумма
currency|string|Валюта
refunded|int|Сумма возврата
description|string|Описание для клиента
wasOpened|bool|Был ли счет открыт в браузере
error|string|Ошибка
comment|string|Комментарий к счету
createdAt|datetime|Дата создания
finishedAt|datetime|Дата завершения

#### InvoiceStatus

 - `active` (Ожидание)
 - `failed` (Ошибка)
 - `successful` (Оплачен)
 - `deleted` (Удален)
 - `cancelled` (Отменен)
 - `returned` (Возврат)
 - `paying` (Оплата)

## Выполнение действий по хуку

Вы можете вернуть список действий, которые необходимо выполнить, в теле ответа. Действия передаются как массив в формате JSON. Можно выполнить не более 10 действий.

Например, вы можете отправить сообщение, текст которого сформирован вашим сервером. Пример ответа:

```
[
{
"type": "sendMessage",
"payload": {
"text": "Текст сообщения"
}
}
]
```

Каждое действие содержит поле `type`, которое определяет его тип. Список возможных действий представлен ниже. Также, с каждым действием может быть указан дополнительный параметр `payload`. Для одних действий параметр обязателен, для других нет.

Действия выполняются в том порядке, в котором указаны, одно за другим, в том контексте, в котором был вызван веб-хук. Если вы неправильно заполнили параметры действия, то оно будет пропущено.